# Challenge-AlgoDS-QRT

This repository contains the solutions we tested for the challenge proposed by QRT for the data ENS competitions.

The training data is located in the data folder.

Many auxiliary functions, introduced by the host of the challenge QRT, in their initial notebook are readapted in python files to help during the estimation of our factors.

We test Factorial Analysis models and Probabilistic PCA models to learn the A matrix, describing the factors used to forecast the values of the financial time series of stock market returns.

These models are implemented in ppca.py and proba_pca.py while the factorial analysis models were imported from scikit-learn. The training and testing of the models are available in the main.py file.

The EM_ppca and ML_ppca packages implements different inference methods (Maximum Likelihood and EM Algorithm) for Probabilistic Principal Component Analysis described by Christopher Bishop.

The report is available in PDF format in the repository, and the link the presentation video is the following : https://drive.google.com/file/d/12_JZT_sBFpq6gOV923WM_SPsqRWaoBF0/view?usp=sharing


