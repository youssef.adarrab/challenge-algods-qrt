import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from metric_train import metric_train
import sklearn
from sklearn.decomposition import FactorAnalysis
from fitBeta import fitBeta
from checkOrthonormality import checkOrthonormality
from sklearn.preprocessing import StandardScaler
from EM_pca import ProbaPCA
from ML_ppca import PPCA
import math



X_train = pd.read_csv('./X_train.csv', index_col=0, sep=',')
X_train.columns.name = 'date'

Y_train = pd.read_csv('./Y_train.csv', index_col=0, sep=',')
Y_train.columns.name = 'date'

print(X_train)

X_train_reshape = pd.concat([ X_train.T.shift(i+1).stack(dropna=False) for i in range(250) ], 1).dropna()
X_train_reshape.columns = pd.Index(range(1,251), name='timeLag')

print(X_train_reshape)

# First implementation of probabilistic pca
ppca = PPCA(q=10)

# First implementation of probabilistic pca
probapca = ProbaPCA(D=10, verbose=True)


# Factor Analysis Model with randomized method fo svd decomposition
fa_svd_randomized = FactorAnalysis(n_components=10, random_state=2, rotation="varimax")

# Factor Analysis Model with lapack method fo svd decomposition (better precision but slower)
fa_svd_lapack = FactorAnalysis(n_components=10, random_state=0, rotation="varimax", svd_method='lapack')

# Scaler to standardize the data
scaler = StandardScaler(with_std=False)
X_train_reshape = scaler.fit_transform(X_train_reshape)


# Fit the data according to the different methods
ppca.fit(X_train_reshape.T)
probapca.fit((X_train_reshape).to_numpy())
fa_svd_randomized.fit(X_train_reshape)
fa_svd_lapack.fit(X_train_reshape)

# First implementation of probabilistic PCA

A_ppca = ppca.w
A_ppca = A_ppca.T
# Gram-schmidt orthogonalization
A_ppca = np.linalg.qr(A_ppca)[0]

# Second implementation of probabilistic PCA
A_probapca = probapca.e_w
A_probapca = A_probapca.T
# Gram-schmidt orthogonalization
A_probapca = np.linalg.qr(A_probapca)[0]

# Factor analysis model (randomized svd)
A_randomized = fa_svd_randomized.components_
A_randomized = A_randomized.T
# Gram-schmidt orthogonalization
A = np.linalg.qr(A)[0]

# Factor analysis model (lapack svd)
A_lapack = fa_svd_lapack.components_
A_lapack = A_lapack.T
# Gram-schmidt orthogonalization
A = np.linalg.qr(A)[0]

Error_ppca = pd.DataFrame(A_ppca.T @ A_ppca - np.eye(10)).abs()
Error_randomized = pd.DataFrame(A_randomized.T @ A_randomized - np.eye(10)).abs()
Error_lapack = pd.DataFrame(A_lapack.T @ A_lapack - np.eye(10)).abs()

print(f"A ppca shape is : {A_ppca.shape}")
print(f"Product of A ppca and A ppca transpose is: {A_ppca.T @ A_ppca}")
print(f"Error on orthogonality is: {Error_ppca.unstack()}")


beta = fitBeta(A_ppca, X_train_reshape, Y_train)
m = metric_train(A_ppca, beta, X_train_reshape, Y_train)

print(f"X shape is {X_train.shape}")
print(f"X reshaped shape is {X_train_reshape.shape}")
print(f"Y shape is {Y_train.shape}")

print(f"A ppca shape is {A_ppca.shape}")

print(f"The A ppca matrix is orthonormal {checkOrthonormality(A_ppca)}")

print(f"The metric train is : {m}")

# from output to csv file...
def parametersTransform(A, beta, D=250, F=10):
    
    if A.shape != (D, F):
        print('A has not the good shape')
        return
    
    if beta.shape[0] != F:
        print('beta has not the good shape')
        return        
    
    output = np.hstack( (np.hstack([A.T, beta.reshape((F, 1))])).T )
    return output

output = parametersTransform(A_ppca, beta)
pd.DataFrame(output).to_csv(path + 'Submission_example.csv')
