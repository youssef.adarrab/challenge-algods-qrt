import numpy as np
from numpy.linalg import inv
from numpy import transpose as tr
import ipdb


class PPCA(object):
    def __init__(self, q=10, sigma=1.0):
        self.q = q
        self.prior_sigma = sigma

    def fit(self, y, em=False):
        self.y = y
        self.p = y.shape[0]
        self.n = y.shape[1]
        [self.w, self.mu, self.sigma] = self.__fit_ml()

    def transform(self, y=None):
        if y is None:
            y = self.y
        [w, mu, sigma] = [self.w, self.mu, self.sigma]
        m = tr(w).dot(w) + sigma * np.eye(w.shape[1])
        m = inv(m)
        x = m.dot(tr(w)).dot(y - mu)
        return x

    def fit_transform(self, *args, **kwargs):
        self.fit(*args, **kwargs)
        return self.transform()

    def transform_infers(self, x=None, noise=False):
        if x is None:
            x = self.transform()
        [w, mu, sigma] = [self.w, self.mu, self.sigma]
        y = w.dot(x) + mu
        if noise:
            for i in range(y.shape[1]):
                e = np.random.normal(0, sigma, y.shape[0])
                y[:, i] += e
        return y

    def __fit_ml(self):
        mu = np.mean(self.y, 1)[:, np.newaxis]
        [u, s, v] = np.linalg.svd(self.y - mu)
        if self.q > len(s):
            ss = np.zeros(self.q)
            ss[:len(s)] = s
        else:
            ss = s[:self.q]
        ss = np.sqrt(np.maximum(0, ss**2 - self.prior_sigma))
        w = u[:, :self.q].dot(np.diag(ss))
        if self.q < self.p:
            sigma = 1.0 / (self.p - self.q) * np.sum(s[self.q:]**2)
        else:
            sigma = 0.0
        return (w, mu, sigma)
